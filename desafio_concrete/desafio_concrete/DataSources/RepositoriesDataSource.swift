//
//  RepositoriesDataSource.swift
//  desafio_concrete
//
//  Created by Rodrigo Eidi Saiki on 23/01/2018.
//  Copyright © 2018 Rodrigo Eidi Saiki. All rights reserved.
//

import UIKit

class RepositoriesDataSource: NSObject, UITableViewDataSource {

    var repItems: [Repository] = []
    
    required init(items: [Repository]) {
        self.repItems = items
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoriesTableViewCell") as! RepositoriesTableViewCell
        cell.prepare(items: repItems[indexPath.row])
        return cell
    }
    
    func numberItems()-> Int{
        return repItems.count
    }
    
    func addItems(newReps: [Repository]){
        for items in newReps{
            self.repItems.append(items)
        }
    }
    
    func getRepInfo(index: Int) -> (repName: String, full_name: String){
        return (repItems[index].name, repItems[index].full_name)
    }
    
    
    

}
