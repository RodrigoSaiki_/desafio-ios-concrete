//
//  PullRequestsDataSource.swift
//  desafio_concrete
//
//  Created by Rodrigo Eidi Saiki on 23/01/2018.
//  Copyright © 2018 Rodrigo Eidi Saiki. All rights reserved.
//

import UIKit

class PullRequestsDataSource: NSObject, UITableViewDataSource {
    
    var itemsPR: [PullRequests] = []
    
    required init(items: [PullRequests]) {
        self.itemsPR = items
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsPR.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PullRequestsTableViewCell") as! PullRequestsTableViewCell
        cell.prepare(pr: itemsPR[indexPath.row])
        return cell
    }
    
    func getPage(index: Int) -> URL{
        return itemsPR[index].page
    }
    
    


}
