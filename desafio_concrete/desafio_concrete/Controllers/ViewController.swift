//
//  ViewController.swift
//  desafio_concrete
//
//  Created by Rodrigo Eidi Saiki on 23/01/2018.
//  Copyright © 2018 Rodrigo Eidi Saiki. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var request = Request()
    var dataSource: RepositoriesDataSource?
    var page: Int = 1
    var refresh: UIRefreshControl!

    enum TypeLoad{
        case loadInitial
        case refresh
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.isHidden = true
        self.indicator.startAnimating()
        loadItems(type: .loadInitial)
        refreshData()
    }
    
    private func loadItems(type: TypeLoad){
        request.loadData(type: .repositories(page: page)) {(items) in
            self.dataSource = RepositoriesDataSource(items: items as! [Repository])
            self.tableView.dataSource = self.dataSource
            
            switch type{
            case .loadInitial:
                animateTable(tableView: self.tableView)
                self.tableView.isHidden = false
                self.indicator.stopAnimating()
                print("Load")
            case .refresh:
                self.tableView.reloadData()
                self.refresh.endRefreshing()
                print("Refresh")
            }

        }
    }

    private func refreshData(){
        refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(actionRefresh), for: .valueChanged)
        self.tableView.addSubview(refresh)
    }
    
    @objc func actionRefresh(){
        page = 1
        loadItems(type: .refresh)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let numberItems = dataSource?.numberItems() else { return }
        if indexPath.row == numberItems - 10 {
            page += 1
            request.loadData(type: .repositories(page: page), complete: { (newItems) in
                self.dataSource?.addItems(newReps: newItems as! [Repository])
                self.tableView.reloadData()
                print("Carregando mais items")
            })
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let prvc = storyboard?.instantiateViewController(withIdentifier: "PullRequestsViewController") as! PullRequestsViewController
        guard let name = dataSource?.getRepInfo(index: indexPath.row).repName,
              let full_name = dataSource?.getRepInfo(index: indexPath.row).full_name
            else { return }
        prvc.nameRep = name
        prvc.full_name = full_name
        self.show(prvc, sender: self)
        
    }
}

