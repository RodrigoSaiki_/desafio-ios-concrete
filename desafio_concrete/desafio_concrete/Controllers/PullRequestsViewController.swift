//
//  PullRequestsViewController.swift
//  desafio_concrete
//
//  Created by Rodrigo Eidi Saiki on 23/01/2018.
//  Copyright © 2018 Rodrigo Eidi Saiki. All rights reserved.
//

import UIKit

class PullRequestsViewController: UIViewController {

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    
    var full_name: String = ""
    var nameRep: String = ""
    var request = Request()
    var dataSource: PullRequestsDataSource?
    var refresh: UIRefreshControl!
    
    enum TypeLoad{
        case loadInitial
        case refresh
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = nameRep
        self.tableView.delegate = self
        self.tableView.isHidden = true
        self.indicator.startAnimating()
        loadItems(type: .loadInitial)
        refreshData()

    }
    private func loadItems(type: TypeLoad){
        request.loadData(type: .pullRequests(full_name: full_name), complete: {(items) in
            if items.count == 0{
                self.alert()
            }else{
                self.dataSource = PullRequestsDataSource(items: items as! [PullRequests])
                self.tableView.dataSource = self.dataSource
                switch type{
                case.loadInitial:
                    animateTable(tableView: self.tableView)
                    self.tableView.isHidden = false
                    self.indicator.stopAnimating()
                case .refresh:
                    self.refresh.endRefreshing()
                    self.tableView.reloadData()
                    print("Refresh")
                }

            }
        })
    }
    
    private func alert(){
        self.indicator.isHidden = true
        let alert = UIAlertController(title: "Repositório vazio", message: "Esse respositório não possui Pull Requests", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {(action) in
            self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true)
    }
    
    
    private func refreshData(){
        refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(actionRefresh), for: .valueChanged)
        self.tableView.addSubview(refresh)
    }
    
    @objc func actionRefresh(){
        loadItems(type: .refresh)
  

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
extension PullRequestsViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let urlPage = dataSource?.getPage(index: indexPath.row) else { return }
        UIApplication.shared.open(urlPage, options: [:])
        
    }
}
