//
//  PullRequestsTableViewCell.swift
//  desafio_concrete
//
//  Created by Rodrigo Eidi Saiki on 23/01/2018.
//  Copyright © 2018 Rodrigo Eidi Saiki. All rights reserved.
//

import UIKit
import SDWebImage
class PullRequestsTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var data: UILabel!
    @IBOutlet weak var photoAuthor: UIImageView!
    @IBOutlet weak var nameAuthor: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func prepare(pr: PullRequests){
        self.title.text = pr.title
        self.body.text = pr.body
        self.data.text = convertDate(dt: pr.data)
        self.nameAuthor.text = pr.author.name
        self.photoAuthor.sd_setImage(with: pr.author.photo)
        self.photoAuthor.roundImage()
        

    }

}
