//
//  RepositoriesTableViewCell.swift
//  desafio_concrete
//
//  Created by Rodrigo Eidi Saiki on 23/01/2018.
//  Copyright © 2018 Rodrigo Eidi Saiki. All rights reserved.
//

import UIKit
import SDWebImage
class RepositoriesTableViewCell: UITableViewCell {
    @IBOutlet weak var nameRep: UILabel!
    @IBOutlet weak var descriptionRep: UILabel!
    @IBOutlet weak var forks: UILabel!
    @IBOutlet weak var stars: UILabel!
    @IBOutlet weak var nameAuthor: UILabel!
    @IBOutlet weak var photoAuthor: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func prepare(items: Repository){
        self.nameRep.text = items.name
        self.descriptionRep.text = items.description
        self.forks.text = String(items.forks)
        self.stars.text = String(items.stars)
        self.nameAuthor.text = String(items.author.name)
        self.photoAuthor.roundImage()
        self.photoAuthor.sd_setImage(with: items.author.photo)
        
    }

}
