//
//  Repositories.swift
//  desafio_concrete
//
//  Created by Rodrigo Eidi Saiki on 23/01/2018.
//  Copyright © 2018 Rodrigo Eidi Saiki. All rights reserved.
//

import Foundation

struct Repository: Codable {
    let name: String
    let full_name: String
    let description: String?
    let author: Author
    let stars: Int
    let forks: Int
    
    enum CodingKeys: String, CodingKey{
        case name
        case full_name
        case description
        case author = "owner"
        case stars = "stargazers_count"
        case forks
    }
}

struct Repositories: Codable{
    let items: [Repository]
}
