//
//  Author.swift
//  desafio_concrete
//
//  Created by Rodrigo Eidi Saiki on 23/01/2018.
//  Copyright © 2018 Rodrigo Eidi Saiki. All rights reserved.
//

import Foundation

struct Author: Codable{
    let name: String
    let photo: URL
    
    enum CodingKeys: String, CodingKey{
        case name = "login"
        case photo = "avatar_url"
    }
}
