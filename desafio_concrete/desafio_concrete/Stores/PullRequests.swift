//
//  PullRequests.swift
//  desafio_concrete
//
//  Created by Rodrigo Eidi Saiki on 23/01/2018.
//  Copyright © 2018 Rodrigo Eidi Saiki. All rights reserved.
//

import Foundation

struct PullRequests: Codable{
    let title: String
    let data: String
    let body:String?
    let author: Author
    let page: URL
    
    enum CodingKeys: String, CodingKey{
        case title
        case data = "created_at"
        case body
        case author = "user"
        case page = "html_url"
        
    }
}
