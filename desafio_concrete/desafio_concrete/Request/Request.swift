//
//  Request.swift
//  desafio_concrete
//
//  Created by Rodrigo Eidi Saiki on 23/01/2018.
//  Copyright © 2018 Rodrigo Eidi Saiki. All rights reserved.
//

import UIKit
import Alamofire


class Request: NSObject {
    
    enum TypeRequest{
        case repositories(page: Int)
        case pullRequests(full_name: String)
        
        func getURL() -> String{
            switch self {
            case .repositories(let page):
                return "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(page)"
            case.pullRequests(let full_name):
                return "https://api.github.com/repos/\(full_name)/pulls"
            }
        }
    }
    
    func loadData(type: TypeRequest, complete: @escaping (([AnyObject])->Void)){
        self.request(type: type) { (data) in
            guard let itemsData = self.getData(type: type, data: data) else { return }
            complete(itemsData)
        }
    }
    
    private func request(type: TypeRequest, callback: @escaping ((Data)->Void)){
        guard let url = URL(string: type.getURL()) else { return }
        Alamofire.request(url).responseData {(response) in
            switch response.result{
            case.success(let data):
                callback(data)
            case .failure(let error):
                print("Erro no request", error)
            }
        }.resume()
    }
    
    private func getData(type: TypeRequest, data: Data) -> [AnyObject]?{
        switch type {
        case .repositories:
            do{
                let repositories = try JSONDecoder().decode(Repositories.self, from: data)
                return repositories.items as [AnyObject]
            }
            catch let jsonErr{
                print("Erro no decoder de repositorios", jsonErr)
            }
        case .pullRequests:
            do{
                let pullRequests = try JSONDecoder().decode([PullRequests].self, from: data)
                return pullRequests as [AnyObject]
            }
            catch let jsonErr{
                print("Erro no decoder de repositorios", jsonErr)
            }
        }
        return nil
    }
    
    

}
