//
//  extensions.swift
//  desafio_concrete
//
//  Created by Rodrigo Eidi Saiki on 23/01/2018.
//  Copyright © 2018 Rodrigo Eidi Saiki. All rights reserved.
//

import UIKit

extension UIImageView{
    func roundImage(){
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
}

func animateTable(tableView: UITableView) {
    tableView.reloadData()
    let cells = tableView.visibleCells
    let tableHeight: CGFloat = tableView.bounds.size.height
    for i in cells {
        let cell: UITableViewCell = i as UITableViewCell
        cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
    }
    var index = 0
    for a in cells {
        let cell: UITableViewCell = a as UITableViewCell
        UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
            cell.transform = CGAffineTransform(translationX: 0, y: 0);
        }, completion: nil)
        index += 1
    }
}

func convertDate(dt: String) -> String {
    let dateFormatter = DateFormatter()
    let tempLocale = dateFormatter.locale
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    guard let date = dateFormatter.date(from: dt) else { return "" }
    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
    dateFormatter.locale = tempLocale
    let dateString = dateFormatter.string(from: date)
    return dateString
}
