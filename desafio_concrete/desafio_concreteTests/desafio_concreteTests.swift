//
//  desafio_concreteTests.swift
//  desafio_concreteTests
//
//  Created by Rodrigo Eidi Saiki on 23/01/2018.
//  Copyright © 2018 Rodrigo Eidi Saiki. All rights reserved.
//

import XCTest
@testable import desafio_concrete

class desafio_concreteTests: XCTestCase {
    var rvc: ViewController!
    var pvc: PullRequestsViewController!
    
    override func setUp() {
        super.setUp()
        rvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
        _ = rvc.view
        
        pvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PullRequestsViewController") as! PullRequestsViewController
        _ = pvc.view
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    func testDidload_rvc(){
        rvc.viewDidLoad()
    }
    func testDidload_pvc(){
        pvc.viewDidLoad()
    }
    
    func testRowHeight_rvc(){
        XCTAssertEqual(rvc.tableView.rowHeight, 132)
    }
    
    func testRowHeight_pvc(){
        XCTAssertEqual(pvc.tableView.rowHeight, 187)
    }
    
    func testNumberOfElementsInTableView_rvc(){
        let rep = Repository(name: "Teste", full_name: "Rodrigo Eidi Saiki", description: "Testando", author:Author(name: "Rodrigo", photo: URL(string: "http")!) , stars: 10, forks: 10)
        
        let teste = [rep,rep,rep]
        
        rvc.dataSource = RepositoriesDataSource(items: teste)
        rvc.tableView.dataSource = rvc.dataSource
        rvc.tableView.reloadData()
        
        XCTAssertEqual(rvc.tableView.numberOfRows(inSection: 0), rvc.dataSource?.numberItems())
        
    }
    
    func testNumberOfElementsInTableView_pvc(){        
        let pulls = PullRequests(title: "Teste", data: "2018/09/09", body: "Teste", author: Author(name:"Rodrigo",photo: URL(string:"http")!), page: URL(string: "https")!)
        
        let teste = [pulls,pulls,pulls,pulls]
        pvc.dataSource = PullRequestsDataSource(items: teste)
        pvc.tableView.dataSource = pvc.dataSource
        pvc.tableView.reloadData()
        
        XCTAssertEqual(pvc.tableView.numberOfRows(inSection: 0), 4)
        
    }
    
}
